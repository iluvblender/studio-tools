# ====================== BEGIN GPL LICENSE BLOCK ======================
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Original code (C) 2020 Blender Foundation.
# ======================= END GPL LICENSE BLOCK ========================

"""Fine-grained Library Overrides.

This is just a snippet of code, not a complete add-on. It can serve as a basis
for an add-on that allows overriding certain properties of arbitrary RNA
properties.
"""

from typing import Any, Dict, Tuple

import bpy

# Mapping from `bl_rna.properties['propname'].XXXX` to their equivalent
# id_datablock['_RNA_UI']['propname']['YYYY'].
_bl_rna_to_rna_ui = {
    "hard_min": "min",
    "hard_max": "max",
    "soft_min": "soft_min",
    "soft_max": "soft_max",
    "subtype": "subtype",
    # 'default' is handled as a special case.
}
_overrides_rna_paths_key = "_overrides_rna_paths"


def _get_property_info(rna_path: str) -> Tuple[Any, Dict]:
    """Given an RNA path to a property, returns info about that property.

    Uses the RNA information in `bl_rna['property_name']` to construct a
    dictionary that is suitable for use in custom properties' `_RNA_UI`.

    :return: tuple (current value, _RNA_UI dict)
    """

    object_path, prop_name = rna_path.rsplit(".", 1)
    py_object = eval(object_path)
    current_value = getattr(py_object, prop_name)

    bl_rna = py_object.bl_rna.properties[prop_name]
    rna_ui = {}
    for bl_rna_attr, rna_ui_key in _bl_rna_to_rna_ui.items():
        rna_ui[rna_ui_key] = getattr(bl_rna, bl_rna_attr)

    if bl_rna.is_array:
        rna_ui["default"] = tuple(bl_rna.default_array)
        current_value = tuple(current_value)
    else:
        rna_ui["default"] = bl_rna.default

    return current_value, rna_ui


def _ensure_properties_for_overrides_exists_on(datablock: Any) -> None:
    """Ensure that _RNA_UI and _overrides_rna_paths exists."""

    if "_RNA_UI" not in datablock:
        datablock["_RNA_UI"] = {}

    if _overrides_rna_paths_key not in datablock:
        datablock[_overrides_rna_paths_key] = {}
        datablock["_RNA_UI"][_overrides_rna_paths_key] = {
            "description": "RNA paths of the overrides",
        }


def add_override(datablock: Any, override_key: str, rna_path: str) -> None:
    """Store an override for the rna_path on the datablock.

    Typically the datablock is the current scene, so that it can be
    iterated over to construct a user interface, write the desired
    overrides to a text datablock, etc.
    """

    current_value, rna_ui = _get_property_info(rna_path)
    if override_key not in datablock:
        datablock[override_key] = current_value

    _ensure_properties_for_overrides_exists_on(datablock)
    datablock["_RNA_UI"][override_key] = rna_ui
    datablock[_overrides_rna_paths_key][override_key] = rna_path


def get_override_mule() -> bpy.types.Object:
    """Create or retrieve the datablock used to store overrides on."""

    object_name = "_override_mule"
    try:
        override_mule = bpy.data.objects[object_name]
    except KeyError:
        override_mule = bpy.data.objects.new(object_name, None)
        # If you want to have the object in the scene:
        # bpy.context.scene.collection.objects.link(override_mule)
    override_mule.use_fake_user = True
    return override_mule


override_mule = get_override_mule()

add_override(
    override_mule,
    "Suzie Base Color",
    "bpy.data.materials['Suzie'].node_tree.nodes['Principled BSDF'].inputs['Base Color'].default_value",
)

add_override(
    override_mule,
    "Suzie SSS Color",
    "bpy.data.materials['Suzie'].node_tree.nodes['Principled BSDF'].inputs['Subsurface Color'].default_value",
)

for override_key, rna_path in override_mule[_overrides_rna_paths_key].items():
    override_value = override_mule[override_key]
    if hasattr(override_value, "to_list"):
        py_value = override_value.to_list()
    elif hasattr(override_value, "to_dict"):
        py_value = override_value.to_dict()
    else:
        py_value = override_value

    print(f"{rna_path} = {py_value}")
